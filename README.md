# Dokumentasi Instalasi Container BKN SIASN

## Instalasi Database (Stage Development)

Sesuaikan username, password, ports yang di expose & volumes pada docker-compose-pgsql12.yml

```sh
version: '3.7'
services:
  app-name:
    image: postgres:12.11-alpine
    restart: always
    environment:
      - POSTGRES_USER=<UBAH USERNAME>
      - POSTGRES_PASSWORD=<UBAH PASSWORD>
    logging:
      options:
        max-size: 10m
        max-file: "3"
    ports:
      - '0.0.0.0:<UBAH PORT>:5432'
    volumes:
      - <UBAH PATH PADA HOST>:/var/lib/postgresql/data
volumes:
  app-name:
    driver: local

```

Apply konfigurasi/build dengan command

```sh
docker-compose -f docker-compose-pgsql12.yml up -d
```
atau
```sh
docker compose -f docker-compose-pgsql12.yml up -d
```

## Instalasi Project (Stage Development)

Versi terbaru Docker & Docker Compose


Membuat file .env dengan copy file .env.dev atau .env.example

```sh
cp .env.dev .env
```
atau
```sh
cp .env.example .env
```

Sesuaikan file .env

Membuat user dengan user **bebas-namanya-apa**

```sh
useradd bebas-namanya-apa
```

Mengambil **UID** user **bebas-namanya-apa**

```sh
id -u bebas-namanya-apa
```

Setup args user & uid pada dev-docker-compose.yml

```sh
version: "3.7"
services:
  app:
    build:
      args:
        user: <UBAH USER SESUAI YANG TELAH DIBUAT>
        uid: <UBAH UID SESUAI DENGAN USER YANG TELAH DI BUAT>
      context: ./
      dockerfile: dev.Dockerfile
    image: bebas-nama-conatiner
    container_name: bebas-nama-conatiner-app
    restart: unless-stopped
    working_dir: /var/www/
    volumes:
      - ./:/var/www
    networks:
      - bebas-nama-conatiner
```

Setup ports yang di expose ke external/host

```sh
  nginx:
    image: nginx:alpine
    container_name: bebas-nama-conatiner-nginx
    restart: unless-stopped
    ports:
      - <UBAH PORT>:80
    volumes:
      - ./:/var/www
      - ./nginx:/etc/nginx/conf.d/
    networks:
      - bebas-nama-conatiner
```

Apply konfigurasi/build dengan command

```sh
docker-compose -f dev-docker-compose.yml up -d
```
atau
```sh
docker compose -f dev-docker-compose.yml up -d
```



